#!/bin/bash
#
# Check if given git project url is found in whitelist file
#
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"

WHITELIST_FILE=$SCRIPT_DIR/git_project_whitelist.txt

if [ ! $# -eq 1 ]; then
    echo "usage: $0 <git_project_url>"
    exit 1
fi
PROJECT_URL=$1


if [ `grep -v '^#' $WHITELIST_FILE | grep "^$PROJECT_URL$"` ]; then
    echo "project url is authorized"
else
    echo "project url is not authorized"
    exit 1
fi