# omi-whitelist

Maintain a list of gitlab project urls and a minimal dockerized tool that checks if a given url belong to this whitelist

Each changes on [git_project_whitelist.txt](git_project_whitelist.txt) file rebuilds the project container image.
